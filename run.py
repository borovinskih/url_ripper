#!/usr/bin/env python
import os
import sys
from multiprocessing import Process
from django.core.management import execute_from_command_line

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "url_ripper.settings")

def run_celery_worker():
	execute_from_command_line(['', 'runserver'])

def run_django_development_server():
	execute_from_command_line(['', 'celery', 'worker'])

if __name__ == "__main__":
    p1 = Process(target=run_celery_worker)
    p2 = Process(target=run_django_development_server)
    p1.start()
    p2.start()
    p1.join()
    p2.join()