;(function (angular) {
    var app = angular.module('urlRipper', []);
    app.controller("MainCtrl", ['$http', '$interval', function ($http, $interval) {
        var ctrl = this;
        ctrl.console = "";
        ctrl.stuff = "";
        $interval(function () {
            $http.get('/data').then(function (response) {
                ctrl.console = response.data.console;
                ctrl.stuff = response.data.stuff;
            });            
        }, 3000);
    }]);
}(angular));