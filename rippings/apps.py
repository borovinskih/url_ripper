from django.apps import AppConfig


class RippingsConfig(AppConfig):
    name = 'rippings'
