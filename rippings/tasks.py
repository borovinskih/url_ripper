from celery import task
from .models import Stuff, Log
import requests
from bs4 import BeautifulSoup

@task
def rip(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')
            stuff = Stuff.objects.get_or_create(url=url)[0]
            stuff.encoding = response.encoding
            stuff.title = soup.title.string
            stuff.header = " ".join(soup.h1.stripped_strings) if soup.h1 else "no h1"
            stuff.save()
            Log.objects.create(url=url, success=True)
        else:
            raise requests.ConnectionError
    except requests.ConnectionError:
        Log.objects.create(url=url, success=False)
