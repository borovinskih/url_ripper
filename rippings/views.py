from django.shortcuts import render
from django.http import HttpResponse
from .models import Stuff, Log
import json

# Create your views here.

def home_page(request):
    return render(request, "index.html")

def data(request):
    result = {
        "stuff": "",
        "console": "",
    };

    for stuff in Stuff.objects.all().order_by('updated_at'):
        result["stuff"] += (stuff.url + " - " + stuff.encoding + " - " + stuff.title + " - " + stuff.header + "\n\n")
    for log in Log.objects.all().order_by('created_at'):
        result["console"] += ("<" + log.created_at.strftime("%d.%m.%Y %H:%M:%S") + "> " + log.url + " - " + ("success" if log.success else "failure") + "\n")

    return HttpResponse(json.dumps(result))