from django.db import models
from django.dispatch import receiver

# Create your models here.

class Victim(models.Model):
    url = models.URLField()
    offset = models.DurationField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.url

class Stuff(models.Model):
    url = models.URLField()
    title = models.CharField(max_length=200)
    encoding = models.CharField(max_length=20)
    header = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Log(models.Model):
    url = models.URLField()
    success = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

@receiver(models.signals.post_save, sender=Victim)
def callback(instance, **kwargs):
    from .tasks import rip
    rip.apply_async((instance.url,), countdown=instance.offset.seconds)
